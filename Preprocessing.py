import pandas as pd
import re
import nltk
from pymorphy2 import MorphAnalyzer
from nltk.corpus import stopwords
from collections import Counter
import enchant
import matplotlib.pyplot as plt
from nltk.tokenize import sent_tokenize
from wordcloud import WordCloud


dictionary = enchant.Dict("ru_RU")
stopwords_ru = stopwords.words("russian")
morph = MorphAnalyzer()
transTable = {ord('y'): 'у', ord('o'): 'о', ord('O'): 'О', ord('M'): 'М', ord('e'): 'е', ord('E'): 'E', ord('p'): 'р',
              ord('P'): 'Р', ord('c'): 'с', ord('C'): 'С', ord('a'): 'а',
              ord('A'): 'A', ord('T'): 'Т', ord('H'): 'Н', ord('x'): 'х', ord('X'): 'Х'}
pos_list = []


def remove_hash_tags(text):
    pattern = r'#[A-Za-zА-Яа-я0-9]*\n*'
    text = re.sub(pattern, '', text)
    return text


def remove_links(text):
    pattern = r"https?://[^,\s]+,?"
    text = re.sub(pattern, '', text)
    return text


def translate(text):
    text = text.translate(transTable)
    return text


def remove_not_kirillian(text):
    pattern = r'[^а-яА-ЯЁё\d\-\s]'
    text = re.sub(pattern, '', text)
    return text


def lemmatize(text: str):
    tokens = []
    for token in text.split():
        if token and token not in stopwords_ru:
            token = morph.normal_forms(token)[0]
            tokens.append(token)
    result_text = ' '.join(tokens)
    if len(result_text.split()) == 0:
        result_text = None
    return result_text


def remove_dates(text: str):
    pattern = r'[0-9]*-[а-я]*'
    text = re.sub(pattern, '', text)
    return text


def remove_newline(text: str):
    pattern = r'[\n\r]'
    text = re.sub(pattern, ' ', text)
    return text


def remove_english_words(text):
    pattern = r'\b[A-Za-z]+\b'
    text = re.sub(pattern, ' ', text)
    return text


def normalize_corpus(corpus, is_lower=True, is_lemmatize=True, is_remove_dates=True, is_remove_newline=True):
    normalized_corpus = []
    for doc in corpus:
        if is_remove_newline:
            doc = remove_newline(doc)
        doc = remove_hash_tags(doc)
        doc = remove_links(doc)
        doc = remove_english_words(doc)
        doc = translate(doc)
        if is_lower:
            doc = doc.lower()
        doc = remove_not_kirillian(doc)
        if is_remove_dates:
            doc = remove_dates(doc)
        if is_lemmatize:
            doc = lemmatize(doc)
        # убираем лишние пробелы
        doc = re.sub(' +', ' ', doc)
        if len(doc.split()) == 0:
            doc = None
        normalized_corpus.append(doc)
    return normalized_corpus


"""
def count_pos(data):
    pos_list = []
    tags = []
    for row in data:
        for token in row.split():
            tags.append(morph.parse(token)[0].tag.POS)
        counts = Counter(tag for tag in tags)
        total = sum(counts.values())
        pos_list.append(dict((word, float(count) / total) for word, count in counts.items()))
        tags = []
    return pos_list
"""


# merged two functions into one to save time
def orthographic_stat_and_pos_list(text):
    count = 0
    tags = []
    for word in text.split():
        if dictionary.check(word):
            count += 1
        tags.append(morph.parse(word)[0].tag.POS)
    counts = Counter(tag for tag in tags)
    total = sum(counts.values())
    pos_list.append(dict((word, float(count) / total) for word, count in counts.items()))
    return count / len(text.split()) * 100


def draw_plots(data_1: pd.DataFrame, data_2: pd.DataFrame):
    plt.style.use('ggplot')
    for i, row_name in enumerate(["lexical_differency", "orthographic_accuracy"]):
        ax_1 = plt.subplot(2, 2, i+1 if i % 2 == 0 else i+2)
        ax_2 = plt.subplot(2, 2, i+2 if i % 2 == 0 else i+3)
        ax_1.scatter(data_1.index, data_1)
        ax_2.hist(data_2, bins=int(100/5), color='blue', edgecolor='black')
        ax_1.set_title(row_name, size=15)
        ax_1.set_xlabel("sample_id", size=10)
        ax_1.set_ylabel(row_name, size=10)
        ax_2.set_title(row_name, size=15)
        ax_2.set_xlabel(row_name, size=10)
        ax_2.set_ylabel("counts", size=10)
    plt.tight_layout()
    plt.show()


def most_common_words(data: pd.DataFrame):
    text_for_show = " ".join(data)
    wordcloud = WordCloud().generate(text_for_show)
    plt.imshow(wordcloud, interpolation='bilinear')
    plt.axis("off")
    plt.show()


def draw_hist_for_sentences(data: pd.DataFrame):
    words_per_sent = []
    for row in data:
        row = remove_hash_tags(row)
        row = remove_links(row)
        row = remove_english_words(row)
        row = translate(row)
        words_per_sent.append(len(sent_tokenize(row)))
    plt.hist(words_per_sent, bins=int(250/25), color='blue', edgecolor='black')
    plt.title('Histogram of word count in each sentence')
    plt.xlabel('length of sentence')
    plt.ylabel('count of sentences')
    plt.show()


def draw_hist_for_texts(data: pd.DataFrame):
    words_per_text = []
    for row in data:
        words_per_text.append(len(row.split()))
    plt.hist(words_per_text, bins=int(2500/100), color='blue', edgecolor='black')
    plt.title('Histogram of word count in each text')
    plt.xlabel('length of text')
    plt.ylabel('count of texts')
    plt.show()


def count_n_grams(data):
    text = " ".join(data)
    tokens = nltk.word_tokenize(text)
    trg = nltk.trigrams(tokens)
    fdist = nltk.FreqDist(trg)
    fdist.plot(20, cumulative=False)


data = pd.read_csv('new_bd.csv', delimiter=',', index_col=0)
stemmed_data = pd.read_csv('stemmed_data.csv', delimiter=',')
data.dropna(subset=['clean_text'], inplace=True)
count_n_grams(data['clean_text'])
"""
# preprocessing data
data['clean_text'] = normalize_corpus(data['text'], is_lemmatize=False, is_remove_dates=False)
data.dropna(subset=['clean_text'], inplace=True)


# processing data
data['lexical_differency'] = data['clean_text'].apply(lambda x: (len(set(x.split())) / len(x.split()) * 100))
data['orthographic_stat'] = data['clean_text'].apply(lambda x: orthographic_stat_and_pos_list(x))


word_count_pos = pd.DataFrame(pos_list).fillna(0)

stemmed_data = pd.DataFrame()
stemmed_data['main_text'] = data['clean_text'].apply(lambda x: lemmatize(x))
stemmed_data.dropna(subset=['main_text'], inplace=True)
stemmed_data['lexical_differency'] = stemmed_data['main_text'].apply(lambda x: len(set(x.split())) / len(x.split()) * 100)
stemmed_data['orthographic_stat'] = stemmed_data['main_text'].apply(lambda x: orthographic_stat_and_pos_list(x))
word_pos_after_stemming = pd.DataFrame(pos_list).fillna(0)


# save data for future testing
word_count_pos.to_csv('word_pos.csv', index=False)
data.to_csv('new_bd.csv', index=False)
word_pos_after_stemming.to_csv('word_pos_after_stemming.csv', index=False)
stemmed_data.to_csv('stemmed_data.csv', index=False)

"""

# draw_plots(data['lexical_differency'], data['orthographic_stat'])
# draw_plots(stemmed_data['lexical_differency'], stemmed_data['orthographic_stat'])

# pd.set_option('display.max_columns', None)
# print(word_count_pos.head())
# print(word_count_pos.describe())
